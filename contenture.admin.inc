<?php


/**
 * @file
 * Admin callbacks for the Contenture module.
 */

/**
 * Implementation of hook_admin_settings()
 */
function contenture_admin_settings_form(&$form_state) {

/*
  $site_id = variable_get('contenture_site_id', '');
  $db_server = variable_get('contenture_db_server', '');
  $region = variable_get('contenture_js_region', 'footer');

*/

  $form['contenture_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Contenture site ID'),
    '#default_value' => variable_get('contenture_site_id', ''),
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['contenture_db_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Contenture database server'),
    '#default_value' => variable_get('contenture_db_server', ''),
    '#description' => t('Note: You should get these values from the account dashboard on !url',
                  array('!url' => l('contenture.com', 'http.contenture.com'))
                ),
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['contenture_javascript_customizations'] = array(
    '#type' => 'textarea',
    '#title' => t('Contenture Customization Javascript Code'),
    '#default_value' => variable_get('contenture_javascript_customizations', CONTENTURE_SAMPLE_JS),
    '#description' => t('Hint: saving code in the database is usually not a good idea. You may extract
    code logic into a javascript file, place it in a theme folder and include from theme and only use
    this place to call appropriate javascript function.'),
    '#rows' => 7,
    '#required' => FALSE,
  );
  
  $form['contenture_js_region'] = array(
    '#type' => 'textfield',
    '#title' => t('Javascript Inclusion Scope'),
    '#default_value' => variable_get('contenture_js_region', 'footer'),
    '#description' => t('The location in which you want to place the script. Possible values are \'header\' and \'footer\' by default. If your theme implements different locations, however, you can also use these. Footer or similar region at the end of the page is highly recommended.'),
    '#size' => 10,
    '#required' => TRUE,
  );


  return system_settings_form($form);
}
